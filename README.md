# $PROJECT_NAME

## Authors

- John Smith
- Jane Doe

## Technologies Used

This application was built using the following technologies:

- Python 3
- Flask
- MySQL

## Description

$PROJECT_NAME is a web application that allows users to do XYZ. It was built as part of a class project at XYZ University.

## Installation Requirements

To install $PROJECT_NAME, you'll need to follow these steps:

1. Clone the repository to your local machine.
2. Install Python 3 and MySQL.
3. Create a virtual environment and activate it.
4. Install the required packages using `pip install -r requirements.txt`.
5. Set up the database by running the SQL script located in `database.sql`.
6. Start the application by running `python app.py`.

## Copyright

$PROJECT_NAME is © 2023 John Smith and Jane Doe. This application is licensed under the MIT License.
